#' YKinseason: Analytical tools for Yukon river inseason analysis.
#'
#' The package comprises functions from several projects that have moderate overlap:
#' \enumerate{
#' \item This is from another package so needs editing.
#' \item Performance analyses of calibration results,
#' \item HRJ data file import, manipulation, and export (revision to the AWK code),
#' \item HRJ data plotting (Not currently included),
#' \item Calculation of the Stratified Proportional Fishery Index (SPFI).
#' \item Calculation of the Fishery Policy (FP) scaler.
#' }
#'
#' @section
#'
#' @import ggplot2
#'
#' @docType package
#' @name YKinseason
NULL
