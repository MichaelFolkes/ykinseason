

#' Title
#'
#' @param data.list A list. Output of \code{\link{readsonarHTML}}.
#' @param passagetable A character vector of length one. The name of the passage table in the list.
#'
#' @return A list.
#' @export
#'
#' @examples
#' \dontrun{
#'
#' #current year of Pilot:
#' #html files are the pilot daily passage:
#' html.filenames <- list.files(path="../data/pilot", pattern = "Passage.*html$", full.names = TRUE)
#' #get the most recent html file:
#' html.filenames.current <- html.filenames[length(html.filenames)]
#' data.currentyear.pilot.list <- readsonarHTML(html.filenames.current)
#' data.currentyear.pilot.list <- extractsonarHTML(data.currentyear.pilot.list)
#' data.currentyear.pilot <- data.currentyear.pilot.list[[1]]$sonar.current.year
#'
#'
#' }
extractsonarHTML <- function(data.list, passagetable=c("Daily Passage By Species", 'Daily Passage By Bank 2020 Eagle Summer Season')){
  #require(XML)
  passagetable <- match.arg(passagetable)

  res <- lapply(data.list, function(tables){
   # browser()
    sonar.current.year <- tables[[passagetable]]

    #remove commas in numbers:
    sonar.current.year[,-1] <- apply(sonar.current.year[,-1],2, function(x) as.integer(gsub(",", "", x)))

    rows.stats <- c("Total", "Var", "se", "cv(percent)", "L90", "U90")
    sonar.stats <- sonar.current.year[ sonar.current.year$Date %in% rows.stats,]
    colnames(sonar.stats)[1] <- "statistic"

    sonar.stats$statistic <- gsub(pattern = "\\(|\\)", replacement = "",sonar.stats$statistic)

    #remove the stats rows from the daily table:
    sonar.current.year <- sonar.current.year[! sonar.current.year$Date %in% rows.stats, ]

    sonar.current.year$od <- calcOrdinalDay(sonar.current.year$Date, format = "%m/%d/%Y")
    #sonar.current.year.total <- sum(sonar.current.year$ALLCHINOOK)

    sonar.current.year$date <- as.Date(calcDate(sonar.current.year$od))

    sonar.stats$date <- max(sonar.current.year$date)
    sonar.stats.wide <- reshape(sonar.stats, direction = 'wide', timevar = 'statistic', idvar = "date")
    row.names(sonar.stats) <- NULL
    row.names(sonar.stats.wide) <- NULL

    return(list(sonar.current.year=sonar.current.year, sonar.stats=sonar.stats, sonar.stats.wide=sonar.stats.wide))

  })
  names(res) <- names(data.list)

  return(res)

}#END extractsonarHTML



reshapeGSIdaily <- function(dat){
  # dat is a data frame

  res2 <- apply(dat,1, function(x){
    dates <- seq(as.Date(x['stratumdatestart']), as.Date(x["stratumdateend"]) , by=1)
    res <- expand.grid(stratum=x['Stratum'], date=dates, mean=as.numeric(x['Mean']), sd=as.numeric(x['SD']))
    res$od <- calcOrdinalDay(res$date)
    res
  })
  do.call('rbind', res2)

} #END reshapeGSIdaily

